package cz.svetonaut.testdatabuilder.generation;

import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import cz.svetonaut.testdatabuilder.context.AnalyzerContext;

import java.util.List;

public class MainGeneratorProcessor {

	private AnalyzerContext context;

	public MainGeneratorProcessor(AnalyzerContext context) {
		this.context = context;
	}

	public void process() {
		final List<ResolvedReferenceTypeDeclaration> dataBuilderClasses = context.getDataBuilderClasses();

		for (ResolvedReferenceTypeDeclaration builderClass : dataBuilderClasses) {
			final BuilderGenerator generator = new BuilderGenerator(builderClass);
			generator.generateBuilder();
		}
	}
}
