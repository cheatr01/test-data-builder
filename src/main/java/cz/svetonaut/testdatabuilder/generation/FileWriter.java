package cz.svetonaut.testdatabuilder.generation;

import com.github.javaparser.ast.CompilationUnit;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class FileWriter {

	public void createFile(CompilationUnit cu, String packageName, String className) {
		final File root = new File("target/generated-sources/" + packageName);
		final File sourceFile = new File(root, className + ".java");
		sourceFile.getParentFile().mkdirs();
		try {
			Files.write(sourceFile.toPath(), cu.toString().getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
