package cz.svetonaut.testdatabuilder.generation;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.expr.ThisExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import cz.svetonaut.testdatabuilder.util.NameUtils;

import java.util.ArrayList;
import java.util.List;

public class BuilderGenerator {

	private ResolvedReferenceTypeDeclaration clazz;
	private FileWriter fileWriter;

	private final List<FieldDeclaration> fields = new ArrayList<>();

	public BuilderGenerator(ResolvedReferenceTypeDeclaration clazz) {
		if (clazz == null) {
			throw new IllegalStateException("Type declaration must not be null!!");
		}
		this.clazz = clazz;
		this.fileWriter = new FileWriter();
	}

	public void generateBuilder() {
		final CompilationUnit cu = new CompilationUnit();
		final String packageName = clazz.getPackageName();
		final String className = clazz.getName() + "Builder";

		final ClassOrInterfaceDeclaration declaration = createClassDeclaration(cu, packageName, className);

		final List<ResolvedFieldDeclaration> dtoFields = clazz.getAllNonStaticFields();

		for (ResolvedFieldDeclaration field : dtoFields) {
			addField(declaration, field);
		}

		for (FieldDeclaration field : fields) {
			field.createGetter();
			addSetter(declaration, field);
		}

		addBuildMethod(declaration);

		fileWriter.createFile(cu, packageName, className);
	}

	private ClassOrInterfaceDeclaration createClassDeclaration(CompilationUnit cu, String packageName, String className) {
		cu.setPackageDeclaration(packageName);

		final ClassOrInterfaceDeclaration declaration = cu.addClass(className);
		declaration.setPublic(true);
		return declaration;
	}

	private FieldDeclaration addField(ClassOrInterfaceDeclaration declaration, ResolvedFieldDeclaration field) {
		final FieldDeclaration fieldDeclaration = declaration.addField(
				field.getType().describe(),
				field.getName(),
				Modifier.Keyword.PRIVATE
		);

		final BuilderFieldInitializer valueInitializer = new BuilderFieldInitializer();
		fieldDeclaration.getVariable(0).setInitializer(valueInitializer.generateTestValue(field).toString());

		fields.add(fieldDeclaration);

		return fieldDeclaration;
	}

//	private void addSetter(ClassOrInterfaceDeclaration declaration,
//	                       FieldDeclaration fieldDeclaration,
//	                       FieldDeclaration staticFieldDeclaration) {
//		final String fieldName = fieldDeclaration.getVariable(0).getNameAsString();
//		final String staticFieldName = staticFieldDeclaration.getVariable(0).getNameAsString();
//		final MethodDeclaration methodDeclaration = declaration.addMethod(
//				fieldName,
//				Modifier.Keyword.PUBLIC
//		);
//
//		methodDeclaration.addParameter(
//				fieldDeclaration.getElementType(),
//				fieldName
//		);
//		methodDeclaration.setType(declaration.getNameAsString());
//
//		final BlockStmt body = methodDeclaration.createBody();
//		final AssignExpr assignExpr = createAssignStmt(fieldName, methodDeclaration);
//		final AssignExpr staticAssignStmt = createAssignStmt(staticFieldName, methodDeclaration);
//
//		final ReturnStmt returnStmt = new ReturnStmt();
//		returnStmt.setExpression(new ThisExpr());
//
//		body.addStatement(assignExpr);
//		body.addStatement(staticAssignStmt);
//		body.addStatement(returnStmt);
//	}

	private void addSetter(ClassOrInterfaceDeclaration declaration,
	                       FieldDeclaration fieldDeclaration) {
		final String fieldName = fieldDeclaration.getVariable(0).getNameAsString();
		final MethodDeclaration methodDeclaration = declaration.addMethod(
				fieldName,
				Modifier.Keyword.PUBLIC
		);

		methodDeclaration.addParameter(
				fieldDeclaration.getElementType(),
				fieldName
		);
		methodDeclaration.setType(declaration.getNameAsString());

		final BlockStmt body = methodDeclaration.createBody();
		final AssignExpr assignExpr = createAssignStmt(fieldName, methodDeclaration);

		final ReturnStmt returnStmt = new ReturnStmt();
		returnStmt.setExpression(new ThisExpr());

		body.addStatement(assignExpr);
		body.addStatement(returnStmt);
	}

	private MethodDeclaration addBuildMethod(
			ClassOrInterfaceDeclaration declaration) {
		final MethodDeclaration methodDeclaration = declaration.addMethod(
				"build",
				Modifier.Keyword.PUBLIC
		);

		methodDeclaration.setType(clazz.getQualifiedName());

		final VariableDeclarationExpr dtoInitialisationStmt = createDtoInicializationStmt();

		final BlockStmt body = methodDeclaration.createBody();
		body.addStatement(dtoInitialisationStmt);

		for (FieldDeclaration field : declaration.getFields()) {
			final NameExpr dto = new NameExpr().setName("dto");
			final MethodCallExpr setterCallExpr = new MethodCallExpr();
			setterCallExpr.setName(NameUtils.getSetterName(field.getVariable(0).getNameAsString()));
			setterCallExpr.setScope(dto);
			setterCallExpr.addArgument(field.getVariable(0).getNameAsString());

			body.addStatement(setterCallExpr);
		}

		final ReturnStmt returnStmt = new ReturnStmt();
		returnStmt.setExpression(new NameExpr().setName("dto"));
		body.addStatement(returnStmt);


		return methodDeclaration;
	}

	private VariableDeclarationExpr createDtoInicializationStmt() {
		final ObjectCreationExpr objectCreationExpr = new ObjectCreationExpr();
		objectCreationExpr.setType(clazz.getQualifiedName());

		final VariableDeclarator variableDeclarator = new VariableDeclarator();
		variableDeclarator.setType(clazz.getQualifiedName());
		variableDeclarator.setName("dto");
		variableDeclarator.setInitializer(objectCreationExpr);

		final VariableDeclarationExpr variableDeclarationExpr = new VariableDeclarationExpr();
		variableDeclarationExpr.addVariable(variableDeclarator);
		return variableDeclarationExpr;
	}

	private AssignExpr createAssignStmt(String fieldName, MethodDeclaration methodDeclaration) {
		final AssignExpr assignExpr = new AssignExpr();
		assignExpr.setTarget(new FieldAccessExpr().setName(fieldName));
		assignExpr.setOperator(AssignExpr.Operator.ASSIGN);
		assignExpr.setValue(new NameExpr(methodDeclaration.getParameter(0).getNameAsString()));
		return assignExpr;
	}
}
