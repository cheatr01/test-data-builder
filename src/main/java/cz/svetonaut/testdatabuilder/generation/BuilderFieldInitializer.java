package cz.svetonaut.testdatabuilder.generation;

import com.github.javaparser.ast.expr.BooleanLiteralExpr;
import com.github.javaparser.ast.expr.CharLiteralExpr;
import com.github.javaparser.ast.expr.DoubleLiteralExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.LongLiteralExpr;
import com.github.javaparser.ast.expr.NullLiteralExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration;
import com.github.javaparser.resolution.types.ResolvedPrimitiveType;
import com.github.javaparser.resolution.types.ResolvedReferenceType;
import com.github.javaparser.resolution.types.ResolvedType;
import cz.svetonaut.testdatabuilder.util.NameUtils;

public class BuilderFieldInitializer {
	private static final String STRING_TYPE = String.class.getName();
	private static final String LONG_TYPE = Long.class.getName();
	private static final String INTEGER_TYPE = Integer.class.getName();
	private static final String BYTE_TYPE = Byte.class.getName();
	private static final String SHORT_TYPE = Short.class.getName();
	private static final String FLOAT_TYPE = Float.class.getName();
	private static final String DOUBLE_TYPE = Double.class.getName();
	private static final String CHAR_TYPE = Character.class.getName();
	private static final String BOOLEAN_TYPE = Boolean.class.getName();

	public Expression generateTestValue(ResolvedFieldDeclaration field) {
		final ResolvedType type = field.getType();
		final String fieldName = field.getName();

		if (type == null) {
			throw new IllegalArgumentException("Field without resolved type");
		}

		String typeQualifiedName;

		if (type.isPrimitive()) {
			final ResolvedPrimitiveType resolvedPrimitiveType = type.asPrimitive();
			typeQualifiedName = resolvedPrimitiveType.getBoxTypeQName();
		} else if (type.isReferenceType()) {
			final ResolvedReferenceType resolvedReferenceType = type.asReferenceType();
			typeQualifiedName = resolvedReferenceType.getQualifiedName();
		} else {
			throw new RuntimeException("Something is wrong!");
		}

		return getExpression(typeQualifiedName, fieldName);
	}

	private Expression getExpression(String typeQualifiedName, String fieldName) {
		if (typeQualifiedName.equals(STRING_TYPE)) {
			return new StringLiteralExpr(NameUtils.dummyficator(fieldName));
		}

		if (typeQualifiedName.equals(LONG_TYPE)) {
			return new LongLiteralExpr("0L");
		}

		if (typeQualifiedName.equals(INTEGER_TYPE)
				|| typeQualifiedName.equals(BYTE_TYPE)
				|| typeQualifiedName.equals(SHORT_TYPE)
		) {
			return new IntegerLiteralExpr("0");
		}

		if (typeQualifiedName.equals(FLOAT_TYPE)) {
			return new DoubleLiteralExpr("0.0f");
		}

		if (typeQualifiedName.equals(DOUBLE_TYPE)) {
			return new DoubleLiteralExpr(0.0);
		}

		if (typeQualifiedName.equals(CHAR_TYPE)) {
			return new CharLiteralExpr('a');
		}

		if (typeQualifiedName.equals(BOOLEAN_TYPE)) {
			return new BooleanLiteralExpr(true);
		}

		return new NullLiteralExpr();
	}
}
