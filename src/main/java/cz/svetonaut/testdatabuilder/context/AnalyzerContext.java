package cz.svetonaut.testdatabuilder.context;

import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AnalyzerContext {

	private List<ResolvedReferenceTypeDeclaration> dataBuilderClasses = new ArrayList<>();

	private List<ResolvedReferenceTypeDeclaration> initializerClasses = new ArrayList<>();

	public void addDataBuilderClass(ResolvedReferenceTypeDeclaration declaration) {
		if (declaration == null) {
			return;
		}
		dataBuilderClasses.add(declaration);
	}

	public void addInitializerClass(ResolvedReferenceTypeDeclaration declaration) {
		if (declaration == null) {
			return;
		}
		initializerClasses.add(declaration);
	}

	public List<ResolvedReferenceTypeDeclaration> getDataBuilderClasses() {
		return Collections.unmodifiableList(dataBuilderClasses);
	}

	public List<ResolvedReferenceTypeDeclaration> getInitializerClasses() {
		return Collections.unmodifiableList(initializerClasses);
	}
}
