package cz.svetonaut.testdatabuilder.analysis;

import com.github.javaparser.ParseResult;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.github.javaparser.utils.SourceRoot;
import cz.svetonaut.testdatabuilder.context.AnalyzerContext;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectProcessor {

	private final Path projectPath;
	private final JavaSymbolSolver javaSymbolSolver;
	private final ClassProcessor classProcessor;
	private final AnalyzerContext context;


	public ProjectProcessor(String projectPath, List<String> paths) throws MojoExecutionException {
		this.projectPath = Path.of(projectPath);
		javaSymbolSolver = getJavaSymbolSolver(this.projectPath, paths);
		classProcessor = new ClassProcessor();
		this.context = new AnalyzerContext();
	}

	public AnalyzerContext process() {
		final SourceRoot sourceRoot = new SourceRoot(projectPath);
		sourceRoot.getParserConfiguration().setSymbolResolver(javaSymbolSolver);
		try {
			final List<ParseResult<CompilationUnit>> parseResults = sourceRoot.tryToParse();
			for (ParseResult<CompilationUnit> it : parseResults) {
				final Optional<CompilationUnit> result = it.getResult();
				if (result.isEmpty()) {
					continue;
				}

				final CompilationUnit cu = result.get();
				classProcessor.processClass(cu, context);

				context.addDataBuilderClass(classProcessor.getDataBuilderDeclaration());
				context.addInitializerClass(classProcessor.getInitializerDeclaration());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return context;
	}

	public static JavaSymbolSolver getJavaSymbolSolver(Path projectPath, List<String> paths) throws MojoExecutionException {
		final ReflectionTypeSolver reflectionTypeSolver = new ReflectionTypeSolver();
		final JavaParserTypeSolver projectTypeSolver = new JavaParserTypeSolver(projectPath);

		final ArrayList<TypeSolver> dependenciesSolvers = new ArrayList<>();
		for (String path : paths) {
			try {
				dependenciesSolvers.add(
						new JarTypeSolver(Path.of(path))
				);
			} catch (IOException e) {
				throw new MojoExecutionException(e.getMessage(), e);
			}
		}

		final CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
		combinedTypeSolver.add(reflectionTypeSolver);
		combinedTypeSolver.add(projectTypeSolver);

		for (TypeSolver typeSolver : dependenciesSolvers) {
			combinedTypeSolver.add(typeSolver);
		}

		final JavaSymbolSolver javaSymbolSolver = new JavaSymbolSolver(combinedTypeSolver);
		StaticJavaParser.getConfiguration().setSymbolResolver(javaSymbolSolver);
		return javaSymbolSolver;
	}
}
