package cz.svetonaut.testdatabuilder.analysis;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.resolution.declarations.ResolvedAnnotationDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import cz.svetonaut.testdatabuilder.Initializer;
import cz.svetonaut.testdatabuilder.TestDataBuilder;
import cz.svetonaut.testdatabuilder.context.AnalyzerContext;

public class ClassProcessor {

	private static final String BUILDER_ANNOTATION_FULL_NAME = TestDataBuilder.class.getName();
	private static final String INIT_FULL_NAME = Initializer.class.getName();

	private ResolvedReferenceTypeDeclaration dataBuilderDeclaration;
	private ResolvedReferenceTypeDeclaration initializerDeclaration;

	public void processClass(CompilationUnit cu, AnalyzerContext context) {
			new ClassVisitor().visit(cu, null);
	}

	public ResolvedReferenceTypeDeclaration getDataBuilderDeclaration() {
		return dataBuilderDeclaration;
	}

	public ResolvedReferenceTypeDeclaration getInitializerDeclaration() {
		return initializerDeclaration;
	}

	private class ClassVisitor extends VoidVisitorAdapter<Void> {

		@Override
		public void visit(ClassOrInterfaceDeclaration cls, Void arg) {
			super.visit(cls, arg);
			final NodeList<AnnotationExpr> annotations = cls.getAnnotations();
			if (annotations.isEmpty()) {
				return;
			}

			for (AnnotationExpr annotation : annotations) {
				final ResolvedAnnotationDeclaration ant = annotation.resolve();
				if (BUILDER_ANNOTATION_FULL_NAME.equals(ant.getQualifiedName())) {
					dataBuilderDeclaration = cls.resolve();
					return;
				}
			}

			final ResolvedReferenceTypeDeclaration resolved = cls.resolve();
			for (var ancestor : resolved.getAllAncestors()) {
				if (INIT_FULL_NAME.equals(ancestor.getQualifiedName())) {
					initializerDeclaration = cls.resolve();
					return;
				}
			}
		}
	}
}
