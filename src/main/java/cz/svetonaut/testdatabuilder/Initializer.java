package cz.svetonaut.testdatabuilder;

public interface Initializer<T> {

	T initialize();
}
