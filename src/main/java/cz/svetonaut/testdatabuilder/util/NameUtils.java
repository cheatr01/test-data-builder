package cz.svetonaut.testdatabuilder.util;

public class NameUtils {

	public static String camelCaseToSnakeCase(String fieldName) {
		String regex = "([a-z])([A-Z]+)";
		String replacement = "$1_$2";

		return fieldName
				.replaceAll(regex, replacement)
				.toUpperCase();
	}

	public static String getSetterName(String fieldName) {
		return "set" + firstToUpperCase(fieldName);
	}

	public static String firstToUpperCase(String fieldName) {
		return fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	public static String dummyficator(String fieldName) {
		return "dummy" + firstToUpperCase(fieldName);
	}
}
