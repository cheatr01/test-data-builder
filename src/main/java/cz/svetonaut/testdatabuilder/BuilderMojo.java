package cz.svetonaut.testdatabuilder;

import cz.svetonaut.testdatabuilder.analysis.ProjectProcessor;
import cz.svetonaut.testdatabuilder.context.AnalyzerContext;
import cz.svetonaut.testdatabuilder.generation.BuilderGenerator;
import cz.svetonaut.testdatabuilder.generation.MainGeneratorProcessor;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mojo(name = "build", defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
public class BuilderMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project}", readonly = true, required = true)
	private MavenProject mavenProject;

	@Parameter(name = "sourcePath", property = "sourcePath", defaultValue = "src/main/java")
	private String sourcePath;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		final String basePath = mavenProject.getBasedir() + "/" + sourcePath;

		final Set<Artifact> dependencies = mavenProject.getArtifacts();

		final List<String> paths = dependencies.stream()
				.map(Artifact::getFile)
				.map(File::toPath)
				.map(Path::toAbsolutePath)
				.map(Path::toString)
				.collect(Collectors.toList());

		log("Process analyzing for basePath " + basePath);

		final ProjectProcessor projectProcessor = new ProjectProcessor(basePath, paths);
		final AnalyzerContext context = projectProcessor.process();

		final MainGeneratorProcessor generatorProcessor = new MainGeneratorProcessor(context);
		generatorProcessor.process();
	}

	public void log(String text) {
		getLog().info(text);
	}
}
